import BrandLogo from "../Assets/brand-logo.png";

export default function Navbar() {
  return (
    <div>
      <nav>
        <div className="logo">
          <img src={BrandLogo} />
        </div>
        <div className="form-search">
          <input type="search" />
          <button>Search</button>
        </div>
        <div className="nav-sign">
          <span>Sign In</span>
        </div>
      </nav>
    </div>
  );
}
