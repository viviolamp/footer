import imgLogo from "../Assets/footer-brand.png";
import imgGplay from "../Assets/googleplay .png";
import imgApp from "../Assets/appstore.png";
import imgFb from "../Assets/face.png";
import imgPin from "../Assets/pinter.png";
import imgIg from "../Assets/instagram.png";

export default function Footer() {
  return (
    <div classNam="footer">
      <div className="footer-container">
        <div className="footer-logo">
          <img src={imgLogo} alt="brand logo" />
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book
          </p>
        </div>
        <ul className="footer-nav">
          <li>Tentang kami</li>
          <li>Blog</li>
          <li>Layanan</li>
          <li>Karir</li>
          <li>Pusat Media</li>
        </ul>
        <div className="social">
          <p>Download</p>
          <div className="social-download">
            <img src={imgGplay} alt="google play" />
            <img src={imgApp} alt="app store" />
          </div>
          <p>Social Media</p>
          <div className="Social-media">
            <img src={imgFb} alt="facebook" />
            <img src={imgPin} alt="pinterest" />
            <img src={imgIg} alt="instagram" />
          </div>
        </div>
      </div>
      <div className="coppyright">
        Copyright 2000-202 MilanTv. All Raight Reserved
      </div>
    </div>
  );
}
