import Navbar from "./Components/Navbar";
import Footer from "./Components/Footer";
// import BrandLogo from "./Assets/brand-logo.png";

export default function App() {
  return (
    <div>
      <Navbar />
      <h1>KONTEN</h1>
      <Footer />
    </div>
  );
}
